libmousex-types-path-class-perl (0.07-2) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing build dependency on libmodule-install-perl.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on perl.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jul 2012 12:43:46 -0600

libmousex-types-path-class-perl (0.07-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 19:43:44 +0100

libmousex-types-path-class-perl (0.07-1) unstable; urgency=low

  [ gregor herrmann ]
  * Remove version from perl (build) dependency.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Imported Upstream version 0.07.
  * Removed README from docs, duplicates POD.
  * Bumped dh compatibility to level 8 (no changes necessary).
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Added copyright paragraphs for included Module::Install and Pod::Markdown.
  * Dropped version on dependencies with no older version in Debian.
  * Added myself to uploaders and copyright.
  * Added libmousex-getopt-perl to B-D-I (additional test) and Suggests.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Wed, 18 Apr 2012 22:53:31 +0200

libmousex-types-path-class-perl (0.06-1) unstable; urgency=low

  * Initial Release. (Closes: #612870)

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 16 Feb 2011 11:02:27 +0900
